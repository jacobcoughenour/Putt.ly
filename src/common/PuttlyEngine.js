'use strict';

const CourseLoader = require('./CourseLoader');
const Player = require('./Player');
const PADDING = 20;

class PuttlyEngine {

    constructor(options) {
        //super(options);
    }

    start() {

        this.worldSettings = {};

        this.on('postStep', () => { this.postStepHandleBall(); });
        this.on('objectAdded', (object) => {
            console.log("objectAdded");
            if (object.id == 1) {
                this.paddle1 = object;
            } else if (object.id == 2) {
                this.paddle2 = object;
            } else if (object.class == Ball) {
                this.ball = object;
            }
        });
    }

    initGame() {
        // create the paddle objects
        console.log('initGame');
        //this.addObjectToWorld(new Paddle(++this.world.idCount, PADDING, 1));
        // this.addObjectToWorld(new Paddle(++this.world.idCount, WIDTH - PADDING, 2));
        // this.addObjectToWorld(new Ball(++this.world.idCount, WIDTH / 2, HEIGHT / 2));
    }

    postStepHandleBall() {

    }

    registerClasses(serializer) {
    }

    processInput(inputData, playerId) {
    }
}

module.exports = PuttlyEngine;
