'use strict';

const THREE = require('three');
const CANNON = require('cannon')

const frameinterval = 30; 
const timeStep = 1/30;
const ballkillheight = -5;

class Physics {

    constructor(gameEngine) {
        //this.gameEngine = gameEngine;
        this.debug = false;

        this.bodys = [];
        this.debugbodys = [];
        this.materials = {};
        this.world;
    }

    initWorld() {

        this.world = new CANNON.World();
        this.world.gravity.set(0,-1,0);
        this.world.broadphase = new CANNON.NaiveBroadphase();
        this.world.solver.iterations = 10;
        this.world.allowSleep = true;
    
        this.bodys = [];
        this.debugbodys = [];
   
        this.materials.ground = new CANNON.Material();
        this.materials.ball = new CANNON.Material();
    
        this.world.addContactMaterial(new CANNON.ContactMaterial(this.materials.ground, this.materials.ball, { friction: 1.0, restitution: 0.4 }));
    
        // if (this.debug) {
        //     cannonDebugRenderer = new THREE.CannonDebugRenderer( scene, world );        
        // }
    
        // if (world.performance != null) {
        //     // info
        //     physicsinfo = document.createElement('div');
        //     physicsinfo.style = "color: #fff; font-size: 0.8em; text-shadow: 0 0 4px #000000; position: fixed; bottom: 8px; left: 8px; cursor: default; user-select: none; opacity: 0.9; z-index: 10000;";
        //     document.body.appendChild(physicsinfo);
        // }
    }

    update() {

    }

}

module.exports = Physics;
