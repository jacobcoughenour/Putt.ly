'use strict';

const THREE = require('three');

const dirs = [0,1, 1,0, 0,-1, -1,0];
const meshdata = {

    // base x and z values of verts
    // 0 - 3
    // | 4 |
    // 1 - 2
    verts: [-0.5,0.5, 0.5,0.5, 0.5,-0.5, -0.5,-0.5, 0,0],

    // y added to verts of each surface corner
    // 0 - 3
    // |(4)|
    // 1 - 2
    sy: [
        [false,false,false,false],
        [false,false,false,false,true],
        [false,true ,false,false],
        [false,true ,false,false],
        [true ,true ,false,false],
        [true ,false,true ,false],
        [true ,false,true ,false],
        [true ,true ,true ,false],
        [true ,true ,true ,false]
    ],

    // surface triangles
    // 0 - 3
    // | 4 |
    // 1 - 2
    stri: [
        [[0,1,3], [1,2,3]],
        [[0,1,4], [1,2,4], [4,3,0], [2,3,4]],
        [[0,1,3], [1,2,3]],
        [[0,2,3], [2,0,1]],
        [[0,2,3], [2,0,1]],
        [[0,2,3], [2,0,1]],
        [[0,1,3], [1,2,3]],
        [[0,2,3], [2,0,1]],
        [[0,1,3], [1,2,3]]
    ],
};

class CourseLoader extends THREE.Object3D {

    constructor(gameEngine) {
        super();

        gameEngine.renderer.scene.add(this);
        this.gameEngine = gameEngine;
        this.worldmesh;
    }

    load(data) {
        console.log(data);

        let blocksx = data.blocks;
        let blockkeysx = Object.keys(blocksx);
    
        let blockgeometry = new THREE.Geometry();
    
        for (let xi = 0; xi < blockkeysx.length; xi++) {
            let xkey = blockkeysx[xi];
            let blocksz = blocksx[xkey];
            let blockkeysz = Object.keys(blocksz);
    
            for (let zi = 0; zi < blockkeysz.length; zi++) {
                let zkey = blockkeysz[zi];
    
                //blocks.push(createblock(xkey, zkey, blocksz[zkey], scene));
                blockgeometry.merge(this.getBlockGeom(xkey, zkey, data.blockdepth, blocksz[zkey], this.getBlockWalls(xkey,zkey,blocksx)));
            }
        }
    
        blockgeometry.mergeVertices();
        blockgeometry.computeFlatVertexNormals();
        blockgeometry.computeBoundingSphere();
    
        this.worldmesh = new THREE.Mesh( blockgeometry, this.gameEngine.renderer.materials.main );
        this.add( this.worldmesh );
        this.worldmesh.castShadow = true;
        this.worldmesh.receiveShadow = true;
        this.worldmesh.matrixAutoUpdate = false;
        this.worldmesh.updateMatrix();
    
        // // edges
        // if (wireframe) {
        //     scene.add( 
        //         new THREE.LineSegments( 
        //             new THREE.WireframeGeometry(blockgeometry), 
        //             wireframematerial
        //         )
        //     );
        // }
    
        // var lbody = new CANNON.Body({
        //     mass: 0, // kg
        //     type: 1,
        //     position: worldmesh.position.toCannon(), // m
        //     shape: TrimeshfromGeo(blockgeometry),
        //     material: groundphymat
        // });
        // addBody(worldmesh, lbody, true);
    
        // ball = new PlayerBall();
        // ball.spawnpoint.set(0,1,0);
        // ball.spawn(scene, worldmesh); 
    
        // camfocusobject = ball;
    
        // //console.log(ball);

        console.log(this);
    }

    ////
    // block functions
    ////

    
    getBlockGeom(x, z, depth, blockdata, walls = [true,true,true,true]) {
    
        blockdata = this.blockdatadefaults(blockdata);
        
        let geometry = new THREE.Geometry();
    
        let sy = meshdata.sy[blockdata.id];
        let tridata = meshdata.stri[blockdata.id];
    
        // surface vertices
        for (let i = 0; i < sy.length; i++) {
            geometry.vertices.push(
                new THREE.Vector3(
                    meshdata.verts[i*2],
                    sy[i] ? 1*blockdata.s : 0,
                    meshdata.verts[i*2+1]
                )
            );
        }
    
        // surface faces
        for (let i = 0; i < tridata.length; i++) {
            geometry.faces.push(
                new THREE.Face3(
                    tridata[i][0], tridata[i][1], tridata[i][2]
                )
            );
        }
    
        // wall vertecies
        let wallvindex = [0,0,0,0];
        for (let wi = 0; wi < 4; wi++) {
            if (walls[wi] || walls[wi == 0 ? 3 : wi-1]) {
                wallvindex[wi] = geometry.vertices.length; // keep track of their index
                geometry.vertices.push(
                    new THREE.Vector3(
                        meshdata.verts[wi*2],
                        -depth-blockdata.y,
                        meshdata.verts[wi*2+1]
                    )
                );
            }
        }
    
        // wall faces
        let o;
        for (let wi = 0; wi < 4; wi++) {
            if (walls[wi]) {
                o = wi == 3 ? 0 : wi+1;
                geometry.faces.push(new THREE.Face3(o, wi, wallvindex[wi]));
                geometry.faces.push(new THREE.Face3(o, wallvindex[wi], wallvindex[o]));
            }
        }
    
        geometry.rotateY(blockdata.r * (0.5*Math.PI));
        geometry.translate(x*1,blockdata.y,z*1);
        
        //geometry.computeVertexNormals(false);
        //geometry.computeFaceNormals();
        //geometry.computeFlatVertexNormals();
        //geometry.computeMorphNormals();
        //geometry.computeBoundingSphere();
    
        return geometry;
    }
    
    getBlockWalls(x, z, blocks) {
        x *= 1;
        z *= 1;
        let xd, zd;
        let sides = [];
        let curblock = blocks[x][z];
    
        for (let i = 0; i < 4; i++) {
            xd = (x + dirs[i*2]).toString();
            zd = (z + dirs[i*2+1]).toString();
    
            sides[i] = !(this.isCoord(xd, zd, blocks) && blocks[xd][zd].y >= curblock.y + (curblock.id <= 1 ? 0 : curblock.s));
        }
    
        return sides;
    }
    
    isCoord(x, z, blocks) {
        return x in blocks && z in blocks[x];
    }
    
    blockdatadefaults(data) {
        return {
            "id": this.defaultr(data["id"], 0),
            "y": this.defaultr(data["y"], 0),
            "s": this.defaultr(data["s"], 0.5),
            "r": this.defaultr(data["r"], 0),
        };
    } 
    
    defaultr(data, defaultval) {
        return data == null ? defaultval : data;
    }

}

module.exports = CourseLoader;
