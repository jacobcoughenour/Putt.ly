'use strict';

const Player = require('../common/Player');

class PuttlyServerEngine {

    constructor(io, gameEngine, options) {
        this.io = io;
        this.gameEngine = gameEngine;
        this.options = options;
    }

    start() {
        this.gameEngine.initGame();

        console.log('serverstart');

        this.players = {};

        this.io.on('connection', this.onClientConnected.bind(this));
    }

    onClientConnected(socket) {

        console.log('Client '+socket.id+' connected');
        this.players[socket.id] = new Player(socket.id);

        socket.emit('loadmap', {url: "http://localhost:3000/map.json"});
        


        
        // send this.players to player that just joined



        socket.on('spawnme', (data) => {
            //console.log('spawnme');
            socket.emit('spawnme', {x: Object.keys(this.players).length, y: 1, z: 0})
            socket.broadcast.emit('playerspawn', {id: socket.id, x: Object.keys(this.players).length-1, y: 1, z: 0});
        });


        socket.on('disconnect', (reason) => {
            console.log('Client '+socket.id+' disconnected: '+reason);
            socket.broadcast.emit('playerdestroy', {});
            this.players[socket.id].destroy();
            delete this.players[socket.id];
        });
        socket.on('disconnect', this.onClientDisconnected.bind(this));

        

        // attach newly connected player an available paddle
        // if (this.players.player1 === null) {
        //     this.players.player1 = socket.id;
        //     this.gameEngine.paddle1.playerId = socket.playerId;
        // } else if (this.players.player2 === null) {
        //     this.players.player2 = socket.id;
        //     this.gameEngine.paddle2.playerId = socket.playerId;
        // }
    }

    onClientDisconnected(reason) {
        console.log(this.players);
    }
}

module.exports = PuttlyServerEngine;
