'use strict';

const ThreeRenderer = require('../client/ThreeRenderer');
const Ball = require('../client/Ball');
const CourseLoader = require('../common/CourseLoader');
const Physics = require('../common/Physics');

class PuttlyClientEngine {

    constructor(gameEngine, options) {

        this.gameEngine = gameEngine;
        this.renderer = new ThreeRenderer();
        this.courseloader = new CourseLoader(this);
        this.physics = new Physics(this);
        this.socket;

        this.playerball;
        this.players = {};

        // camera
        this._orbitspeed = 0.01;
        this._mousestart;
        this._aiming = false;
        this._aimdist = 0;

        window.addEventListener('wheel', this.onMouseWheel.bind(this), false);
        window.addEventListener('mousemove', this.onMouseMove.bind(this), false);
        window.addEventListener('mousedown', this.onMouseDown.bind(this), false);
        window.addEventListener('mouseup', this.onMouseUp.bind(this), false);
        window.addEventListener('contextmenu', this.onContextMenu.bind(this), false);
    }

    start() {
        this.socket = io.connect(); // connect to server
        this.socket.on('loadmap', this.onLoadMap.bind(this));
        this.socket.on('playerspawn', this.onPlayerSpawn.bind(this));

        this.playerball = new Ball(this);
        this.renderer.scene.add(this.playerball);

        this.socket.on('spawnme', (data) => {
            this.playerball.spawnpoint.set(data.x, data.y, data.z);
            this.playerball.spawn();
        });

        this.renderer.drawSky();

        this.update();
    }

    update() {
        requestAnimationFrame(() => this.update());
        this.renderer.draw();
    }

    ////
    // Sockets
    ////

    onLoadMap(data) {
        //console.log("loadmap "+ data.url);

        // get map json data from url
        this.getJSON(data.url, (err, mdata) => {
            if (err !== null)
                console.log('Failed to fetch map data: ' + err);
            else
                this.courseloader.load(mdata);

            this.socket.emit('spawnme', {});
            //this.playerball.spawn();
        });
    }

    onPlayerSpawn(data) {
        //console.log("playerspawn");

        this.players[data.id] = new Ball(this);
        let ball = this.players[data.id];
        this.renderer.scene.add(ball);

        ball.spawnpoint.set(data.x,data.y,data.z);
        ball.spawn();
    }

    getJSON(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function() {
            var status = xhr.status;
            if (status === 200) {
                callback(null, xhr.response);
            } else {
                callback(status, xhr.response);
            }
        };
        xhr.send();
    }

    ////
    // Controls
    ////

    onMouseWheel(e) {
        this.renderer.camerarig.zoom(e.deltaY);
    }

    onMouseMove(e) {
        if (e.buttons == 1) { // pan

            // if (camfocusobject != null) {
            //     if (camfocusobject.type == 'PlayerBall' && camfocusobject.body.sleepState == 2) {
            //         aiming = true;
            //         aimdist = Math.max(-(mousedownstart - e.clientY), 0);
            //         camfocuspoint.rotation.y -= e.movementX * orbitspeed;
            //     } else {
            //         return;
            //     }
            // }
            this.renderer.camerarig.pan(e.movementX, e.movementY);

        } else if (e.buttons == 2) { // rotate
            this.renderer.camerarig.rotate(e.movementX * this._orbitspeed);
        } else if (e.buttons == 3) { // zoom
            this.renderer.camerarig.zoom(e.movementY*5);
        }
    }

    onMouseDown(e) {
        this._mousestart = e.clientY;
    }

    onMouseUp(e) {
    }

    onContextMenu(e) {
        // disable context menu
        e.preventDefault();
        return false;
    }
}

module.exports = PuttlyClientEngine;
