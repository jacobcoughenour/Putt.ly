'use strict';

const THREE = require('three');

class Ball extends THREE.Object3D {

    constructor(gameEngine) {
        super();
        this.type = 'Ball';
        this.gameEngine = gameEngine;

        this.size = 0.05;
        this.spawnpoint = new THREE.Vector3(0,0,0);
        
        this.mesh;
        this.body;
        this.shadowdecal;
        this.worldmesh;
        this.shadowpos;
    }

    spawn() {

        this.position.copy(this.spawnpoint);

        let geo = new THREE.SphereBufferGeometry(this.size, 16, 8);

        this.mesh = new THREE.Mesh(geo, this.gameEngine.renderer.materials.ball);

        this.add(this.mesh);
    }

    hit() {
        
    }

}

module.exports = Ball;
