'use strict';

const THREE = require('three');
const Stats = require('stats.js');

const CameraRig = require('./CameraRig');

const lightdist = 128;
const shadowresolution = 2048;

class ThreeRenderer {

    constructor(gameEngine, clientEngine) {

        this.gameEngine = gameEngine;
        this.clientEngine = clientEngine;

        // light paramaters for dat.gui
        this.lightparams = {
            skytop: '#373970',
            skybottom: '#49bba0',
            skytopratio: 0.3,
            skytoppos: 0,
            skybottomratio: 0.6,
            skybottompos: 1,
            skyrady0: 130,
            skyradr0: 200,
            skyradr1: 0,
            shadowcolor: '#373970',
            topcolor1: '#22e8b9',
            topcolor2: '#4999a0',
            color1: '#4999a0',
            color2: '#49bba0',
            colormaxy: 4,
            colorminy: -16,
        };

        // main scene to render
        this.scene = new THREE.Scene();

        // init camera rig
        this.camerarig = new CameraRig(this.scene);
        this._camera = this.camerarig.camera;

        // init textures
        let balltexture = new THREE.TextureLoader().load( "/texture/ball.png" );
        balltexture.wrapS = THREE.RepeatWrapping;
        balltexture.wrapT = THREE.RepeatWrapping;
        // balltexture.magFilter = THREE.NearestFilter;
        // balltexture.minFilter = THREE.NearestFilter;
        balltexture.repeat.set( 1, 1 );

        // init materials
        this.materials = {};

        this.materials.main = new THREE.ShaderMaterial({
            vertexShader: this.shaderParse([
                "#include <shadowmap_pars_vertex>",
                "varying vec3 vNormal;",
                "varying vec4 vWorldPosition;",
                "varying vec2 vUv;",
                "void main() {",
                    "#include <begin_vertex>",
                    "#include <project_vertex>",
                    "#include <worldpos_vertex>",
                    "vWorldPosition = worldPosition;",
                    "vNormal = normal;",
                    "vUv = uv;",
                    "#include <shadowmap_vertex>",
                "}"
            ]),
            fragmentShader: this.shaderParse([
                "precision lowp float;",
                "uniform vec3 shadowcolor;",
                "uniform float opacity;",
                "uniform vec3 topcolor1;",
                "uniform vec3 topcolor2;",
                "uniform float topcolordiff;",
                "uniform vec3 color1;",
                "uniform float miny;",
                "uniform vec3 color2;",
                "uniform float maxy;",
                "uniform vec3 lightPosition;",
                "varying vec3 vNormal;",
                "varying vec4 vWorldPosition;",
                "#include <common>",
                "#include <packing>",
                "#include <bsdfs>",
                "#include <lights_pars>",
                "#include <shadowmap_pars_fragment>",
                "#include <shadowmask_pars_fragment>",

                "void main() {",
                    "vec3 color;",
                    "if (vNormal.y > 0.0) {",
                        "color = mix(topcolor2, topcolor1, vNormal.y);",
                        "color = color * (1.0 - (mod(floor(vWorldPosition.x*2.0) + mod(floor(vWorldPosition.z*2.0), 2.0), 2.0) * topcolordiff));",
                    "} else {",
                        "color = mix(color2, color1, max(min((vWorldPosition.y - miny) / (maxy - miny), 1.0), 0.0));",
                    "}",
                    "float shadowamount = (1.0 - min(getShadowMask()*10.0, 1.0)) * opacity;",
                    "float backface = min(max(-dot(vNormal, normalize(lightPosition)), 0.0), 1.0) * 1.0;",

                    "gl_FragColor = vec4( mix(shadowcolor, color, max(1.0 - backface - shadowamount, 0.0)), 1.0);",
                    //"gl_FragColor = vec4( color, 1.0);",
                "}"
            ]),
            uniforms: THREE.UniformsUtils.merge([THREE.ShaderLib['shadow'].uniforms, {
                opacity			: { type : "f", value : 0.15 },
                shadowcolor 	: { type : "c", value : new THREE.Color(this.lightparams.shadowcolor) },
                topcolor1 		: { type : "c", value : new THREE.Color(this.lightparams.topcolor1) },
                topcolor2 		: { type : "c", value : new THREE.Color(this.lightparams.topcolor2) },
                topcolordiff 	: { type : "f", value : 0.04 },
                color1			: { type : "c", value : new THREE.Color(this.lightparams.color1) },
                color2 			: { type : "c", value : new THREE.Color(this.lightparams.color2) },
                maxy 			: { type : "f", value : this.lightparams.colormaxy },
                miny 			: { type : "f", value : this.lightparams.colorminy },
                lightPosition	: {	type : "v3",value : new THREE.Vector3(1,1,1) }
            }])
        });
        this.materials.main.setValues({
            lights: true,
            fog: false,
            transparent: false
        });

        this.materials.ball = new THREE.ShaderMaterial({
            vertexShader: this.shaderParse([
                "varying vec2 vUv;",
                "void main() {",
                    "vUv = uv;",
                    "gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);",
                "}"
            ]),
            fragmentShader: this.shaderParse([
                "precision lowp float;",
                "uniform sampler2D texture1;",
                "uniform vec3 color;",
                "uniform vec3 dotcolor;",
                "uniform float opacity;",
                "uniform float dotopacity;",
                "varying vec2 vUv;",
                "void main() {",
                    "gl_FragColor = vec4(mix(color, dotcolor, dotopacity-(texture2D(texture1, vUv).x*dotopacity)), opacity);",
                "}"
            ]),
            uniforms: {
                color		: { type : "f", value : new THREE.Color("#ffffff") },
                dotcolor	: { type : "f", value : new THREE.Color(this.lightparams.shadowcolor) },
                texture1	: { type : "t", value : balltexture },
                opacity		: { type : "f", value : 1.0 },
                dotopacity	: { type : "f", value : 0.05 },
            }
        });
        this.materials.ball.setValues({
            lights: false,
            fog: false,
            transparent: false
        });


        // init lighting
        this.dirLight = new THREE.DirectionalLight(0xffffff, 1);
        this.dirLight.color.setHSL( 0.1, 1, 0.95 );
        this.dirLight.position.set( -1, 1, 1 );
        this.materials.main.uniforms.lightPosition.value = this.dirLight.position;
        this.dirLight.castShadow = true;
        this.dirLight.shadow.mapSize.width = this.dirLight.shadow.mapSize.height = shadowresolution;
        this.dirLight.shadow.camera.far = lightdist;	
        this.dirLight.shadow.camera.near = -lightdist;
        this.scene.add( this.dirLight );

        // init THREE.js WebGLRenderer
        this.renderer = new THREE.WebGLRenderer({ alpha: true });
        this.renderer.setSize(window.innerWidth, window.innerHeight);

        // shadowmap settings
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.renderReverseSided = false;
        //this.renderer.shadowMap.renderSingleSided = true;
        //this.renderer.shadowMap.type = THREE.BasicShadowMap;
        this.renderer.shadowMap.type = THREE.PCFShadowMap;
        //this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        // add it to the DOM
        document.getElementById('viewportwrapper').appendChild(this.renderer.domElement);

        // init sky
        let sky = document.getElementById('skybg');
        if (sky.getContext) {
            this.skyctx = sky.getContext('2d');
        }

        // stats
        let container = document.createElement('div');
        document.body.appendChild(container);
        this.stats = new Stats();
        container.appendChild(this.stats.dom);

        // fade in body with css transition
        document.body.classList.add('maploaded');

        // handle Window resizing
        window.addEventListener('resize', this.onWindowResize.bind(this), false);
    }

    draw() {
        this.renderer.render(this.scene, this._camera);
        this.stats.update();
    }

    /**
     * Draws/Updates the sky canvas.
     * Call after changing this.lightparams.sky values.
     */
    drawSky() {
        if (this.skyctx == null || this.lightparams == null) return;

        this.skyctx.clearRect(0, 0, 100, 100);
        let gradient = this.skyctx.createRadialGradient(50, this.lightparams.skyrady0+50, this.lightparams.skyradr0, 50, this.lightparams.skyrady0+50, this.lightparams.skyradr1);
        gradient.addColorStop(this.lightparams.skytopratio, this.lightparams.skytop);
        gradient.addColorStop(this.lightparams.skybottomratio, this.lightparams.skybottom);
        this.skyctx.fillStyle = gradient;
        this.skyctx.fillRect(0, 0, 100, 100);
    }

    /**
     * Handle the addition of a new object to the world.
     * @param {Object} obj - The object to be added.
     */
    addObject(obj) {
    }

    /**
     * Handle the removal of an old object from the world.
     * @param {Object} obj - The object to be removed.
     */
    removeObject(obj) {
    }

    onWindowResize() {
        this.camerarig.updateAspect();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
    }

    shaderParse(glsl) {
        return glsl.join("\n");
    }
}

module.exports = ThreeRenderer;
