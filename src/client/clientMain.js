const PuttlyClientEngine = require('../client/PuttlyClientEngine');
const PuttlyEngine = require('../common/PuttlyEngine');

// create a client engine and a game engine
const gameEngine = new PuttlyEngine();
const clientEngine = new PuttlyClientEngine(gameEngine);

document.addEventListener('DOMContentLoaded', function(e) { clientEngine.start(); });
