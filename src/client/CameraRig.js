'use strict';

const THREE = require('three');
const vectorup = new THREE.Vector3(0, 1, 0);

class CameraRig {

    /**
     * @param {THREE.Object3D} scene - parent scene
     */
    constructor(scene) {

        this._zoom = 4;
        this.zoomspeed = 0.1;
        this.zoommax = 200;
        this.zoommin = 1;

        let aspect = window.innerWidth / window.innerHeight;
        this.camera = new THREE.OrthographicCamera(this._zoom * aspect * -0.5, this._zoom * aspect * 0.5, this._zoom * 0.5, this._zoom * -0.5, -128, 128);

        this.camfocuspoint = new THREE.Group();
        this.camfocuspoint.position.set(0, 0, 0);
        this.camfocuspoint.rotation.set(0, Math.PI * 0.25, 0);
        scene.add(this.camfocuspoint);

        this.camfocusrotation = new THREE.Group();
        this.camfocusrotation.position.set(0, 0, 0);
        this.camfocusrotation.rotation.set(-30 * (Math.PI / 180), 0, 0);
        this.camfocusrotation.add(this.camera);
        this.camfocuspoint.add(this.camfocusrotation);

        this.camera.position.set(0, 0, 0);
    }

    /**
     * Updates the camera aspect ratio and projection matrix.
     * Call when the aspect, resolution, or zoom is changed.
     */
    updateAspect() {
        let aspect = window.innerWidth / window.innerHeight;
        this.camera.left = this._zoom * aspect * -0.5;
        this.camera.right = this._zoom * aspect * 0.5;
        this.camera.top = this._zoom * 0.5;
        this.camera.bottom = this._zoom * -0.5;
        this.camera.updateProjectionMatrix();
    }

    /**
     * Camera zoom control.
     * clamps to CameraRig.zoommax and CameraRig.zoommin.
     * @param {number} amount - relative zoom amount to apply
     */
    zoom(amount) {
        this._zoom = Math.max(this.zoommin, Math.min(this.zoommax, this._zoom + ((amount * (this._zoom/this.zoommax)) * this.zoomspeed)));
        this.updateAspect();
    }

    /**
     * Camera pan control.
     * send raw mouse movement
     * @param {number} x - mouse movement delta x
     * @param {number} y - mouse movement delta y
     */
    pan(x, y) {
        this.camfocuspoint.position.add(this.screenToWorldScale(-x,-y));
    }

    /**
     * Camera rotation control.
     * @param {number} r - relative rotation amount
     */
    rotate(r) {
        this.camfocuspoint.rotation.y -= r;
    }
    
    /**
     * Scales screen coordinate to world.
     * Used for panning the camera.
     * @param {number} x
     * @param {number} y 
     * @return {THREE.Vector3}
     */
    screenToWorldScale(x, y) {
        // x /= window.innerWidth;
        // y /= window.innerHeight;

        // x *= camera.right*2;
        // y *= camera.top*2;

        // y /= -Math.sin(camfocusrotation.rotation.x);

        // applyAxisAngle of camera rotation

        return (new THREE.Vector3((x / window.innerWidth) * this.camera.right*2, 0, ((y / window.innerHeight) * this.camera.top*2) / -Math.sin(this.camfocusrotation.rotation.x))).applyAxisAngle(vectorup, this.camfocuspoint.rotation.y);
    }
}

module.exports = CameraRig;
