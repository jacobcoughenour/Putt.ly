
function getBlockGeom(x, z, depth, blockdata, walls = [true,true,true,true]) {

    blockdata = blockdatadefaults(blockdata);
    
    var geometry = new THREE.Geometry();

    var sy = meshdata.sy[blockdata.id];
    var tridata = meshdata.stri[blockdata.id];

    // surface vertices
    for (var i = 0; i < sy.length; i++) {
        geometry.vertices.push(
            new THREE.Vector3(
                meshdata.verts[i*2],
                sy[i] ? 1*blockdata.s : 0,
                meshdata.verts[i*2+1]
            )
        );
    }

    // surface faces
    for (var i = 0; i < tridata.length; i++) {
        geometry.faces.push(
            new THREE.Face3(
                tridata[i][0], tridata[i][1], tridata[i][2]
            )
        );
    }

    // wall vertecies
    var wallvindex = [0,0,0,0];
    for (var wi = 0; wi < 4; wi++) {
        if (walls[wi] || walls[wi == 0 ? 3 : wi-1]) {
            wallvindex[wi] = geometry.vertices.length; // keep track of their index
            geometry.vertices.push(
                new THREE.Vector3(
                    meshdata.verts[wi*2],
                    -depth-blockdata.y,
                    meshdata.verts[wi*2+1]
                )
            );
        }
    }

    // wall faces
    var o;
    for (var wi = 0; wi < 4; wi++) {
        if (walls[wi]) {
            o = wi == 3 ? 0 : wi+1;
            geometry.faces.push(new THREE.Face3(o, wi, wallvindex[wi]));
            geometry.faces.push(new THREE.Face3(o, wallvindex[wi], wallvindex[o]));
        }
    }

    geometry.rotateY(blockdata.r * (0.5*Math.PI));
    geometry.translate(x*1,blockdata.y,z*1);



    
    //geometry.computeVertexNormals(false);
    //geometry.computeFaceNormals();
    //geometry.computeFlatVertexNormals();
    //geometry.computeMorphNormals();
    //geometry.computeBoundingSphere();

    return geometry;
}

const dirs = [0,1, 1,0, 0,-1, -1,0];
function getBlockWalls(x, z, blocks) {
    x *= 1;
    z *= 1;
    var xd, zd;
    var sides = [];
    var curblock = blocks[x][z];

    for (var i = 0; i < 4; i++) {
        xd = (x + dirs[i*2]).toString();
        zd = (z + dirs[i*2+1]).toString();

        sides[i] = !(isCoord(xd, zd, blocks) && blocks[xd][zd].y >= curblock.y + (curblock.id <= 1 ? 0 : curblock.s));
    }

    return sides;
}

function isCoord(x, z, blocks) {
    return x in blocks && z in blocks[x];
}

function blockdatadefaults(data) {
    return {
        "id": defaultr(data["id"], 0),
        "y": defaultr(data["y"], 0),
        "s": defaultr(data["s"], 0.5),
        "r": defaultr(data["r"], 0),
    };
} 

function defaultr(data, defaultval) {
    return data == null ? defaultval : data;
}