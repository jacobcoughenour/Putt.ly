var bodys = [];
var debugbodys = [];
var groundphymat;
var ballphymat;
const physicsdebug = false;

function initPhysics(){

    world = new CANNON.World();
    world.gravity.set(0,-1,0);
    world.broadphase = new CANNON.NaiveBroadphase();
    world.solver.iterations = 10;
    world.allowSleep = true;

    bodys = [];
    debugbodys = [];

    groundphymat = new CANNON.Material();
    ballphymat = new CANNON.Material();

    world.addContactMaterial(new CANNON.ContactMaterial(groundphymat, ballphymat, { friction: 1.0, restitution: 0.4 }));

    if (physicsdebug) {
        cannonDebugRenderer = new THREE.CannonDebugRenderer( scene, world );        
    }

    // if (world.performance != null) {
    //     // info
    //     physicsinfo = document.createElement('div');
    //     physicsinfo.style = "color: #fff; font-size: 0.8em; text-shadow: 0 0 4px #000000; position: fixed; bottom: 8px; left: 8px; cursor: default; user-select: none; opacity: 0.9; z-index: 10000;";
    //     document.body.appendChild(physicsinfo);
    // }
}

var last = 0;
const frameinterval = 30; 
const timeStep = 1/30;
const ballkillheight = -5;

function updatePhysics() {
    if (world == null) return;

    world.step(timeStep);

    for (var i = 0; i < bodys.length; i++) {
        var mesh = bodys[i][0];
        mesh.position.copy(bodys[i][1].position);
        mesh.quaternion.copy(bodys[i][1].quaternion);
        if (mesh.type == 'PlayerBall' ) {
            if (mesh.position.y < ballkillheight) {
                mesh.respawn();
            }
            mesh.update();
        }
    }

    if (physicsdebug) cannonDebugRenderer.update();

    // only every (frameinterval) frames
    if (last < frameinterval) {
        last++; return;
    }
    last = 0;

    //console.log(renderer.info.render.vertices);
    
    
    //if (physicsinfo == null || world.performance == null) return;
    //physicsinfo.innerHTML = world.performance.show();
}

function addBody(mesh, body, static = false, debug = false) {
    world.addBody(body);
    if (static) return;
    bodys.push([mesh, body]);
    if (debug) {
        debugbodys.push([mesh, body]);
        body.addEventListener("sleepy",function(event){
            console.log("sleepy");
        });  
        body.addEventListener("sleep",function(event){
            console.log("sleep");
        });
        body.addEventListener("wakeup",function(event){
            console.log("wakeup");
        });
    }
}

function TrimeshfromGeo(geometry) {
    var vertices = [];
    var indices = [];

    for (var g = 0; g < geometry.vertices.length; g++) {
        var v = geometry.vertices[g];
        vertices.push(v.x);
        vertices.push(v.y);
        vertices.push(v.z);
    }
    for (var i = 0; i < geometry.faces.length; i++) {
        var f = geometry.faces[i];
        indices.push(f.a);
        indices.push(f.b);
        indices.push(f.c);
    }

    return new CANNON.Trimesh(vertices, indices);    
}