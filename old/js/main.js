var scene, level, camera, renderer, canvas, skybg, skybgctx, world, composer, ball;
var geometry, mesh, worldmesh;
var hemiLight, hemiLightHelper, dirLight, dirLightHeper, cannonDebugRenderer;
var stats, physicsinfo;
var materials = {};
var mainmaterial, wireframematerial, ballmaterial, ballshadowmaterial;
var wireframe = false;

THREE.Vector3.prototype.toCannon = function() {
    return new CANNON.Vec3(this.x, this.y, this.z);
};
CANNON.Vec3.prototype.toThree = function() {
    return new THREE.Vector3(this.x, this.y, this.z);
};

const mapdata = {
	blocks : {
		"0": {"-1" : {"id": 0, "y": 0, "e":"hole"},"0" : {"id": 0, "y": 0},"1" : {"id": 0, "y": 0}},
		"1": {"-1" : {"id": 0, "y": 0},"0" : {"id": 0, "y": 0},"1" : {"id": 1, "y": 0, "s": -0.1}},
		"2": {"-1" : {"id": 0, "y": 0},"0" : {"id": 0, "y": 0},"1" : {"id": 2, "y": 0}},
		"3": {"-1" : {"id": 0, "y": 0},"0" : {"id": 4, "y": 0, "r": 1},"1" : {"id": 0, "y": 0.5}},
		"4": {"-1" : {"id": 0, "y": 0.5},"0" : {"id": 0, "y": 0.5},"1" : {"id": 0, "y": 0.5}},
		"5": {"-1" : {"id": 0, "y": 0.5},"0" : {"id": 0, "y": 0},"1" : {"id": 5, "y": 0}},
		"6": {"-1" : {"id": 0, "y": 0.5},"0" : {"id": 0, "y": 0},"1" : {"id": 6, "y": 0}},
		"7": {"-1" : {"id": 0, "y": 0.5},"0" : {"id": 0, "y": 0},"1" : {"id": 7, "y": 0}},
	},
	blockdepth: 32
};

var zoomfustrum = 4;
var camfocuspoint;
var camfocusrotation;
var camfocusobject = null;

var lightparams = {

	// sunlightcolor: "#373970",
	// amblightcolor: "#49bba0",
	skytop: "#373970",
	skybottom: "#49bba0",
	skytopratio: 0.3,
	skytoppos: 0,
	skybottomratio: 0.6,
	skybottompos: 1,
	rady0: 130,
	radr0: 200,
	radr1: 0,

	shadowcolor: "#373970",
	topcolor1: "#22e8b9",
	topcolor2: "#4999a0",
	color1: "#4999a0",
	color2: "#49bba0",
	colormaxy: 4,
	colorminy: -16,
};
var gameparams = {

	focusonball: true,

};


init();
animate();

function init() {

	// sky bg
	skybg = document.getElementById("skybg");  
	// @ts-ignore
	if (skybg.getContext) {  
		// @ts-ignore
		skybgctx = skybg.getContext("2d");
	}


	// @ts-ignore	
	var gui = new dat.GUI();

	var f1 = gui.addFolder('sky');
	// gui.addColor(lightparams, 'sunlightcolor');
	// gui.addColor(lightparams, 'amblightcolor');
	f1.addColor(lightparams, 'skytop').onChange(updateBgSky);
	f1.addColor(lightparams, 'skybottom').onChange(updateBgSky);
	f1.add(lightparams, 'skytopratio', 0, 1).onChange(updateBgSky);
	f1.add(lightparams, 'skybottomratio', 0, 1).onChange(updateBgSky);
	f1.add(lightparams, 'rady0', 0, 200).onChange(updateBgSky);
	f1.add(lightparams, 'radr0', 0, 200).onChange(updateBgSky);
	f1.add(lightparams, 'radr1', 0, 200).onChange(updateBgSky);

	var f2 = gui.addFolder('uniforms');	
	f2.addColor(lightparams, 'shadowcolor').onChange(updateUniforms);
	f2.addColor(lightparams, 'topcolor1').onChange(updateUniforms);
	f2.addColor(lightparams, 'topcolor2').onChange(updateUniforms);
	f2.addColor(lightparams, 'color1').onChange(updateUniforms);
	f2.addColor(lightparams, 'color2').onChange(updateUniforms);
	f2.add(lightparams, 'colormaxy').onChange(updateUniforms);
	f2.add(lightparams, 'colorminy').onChange(updateUniforms);

	var f3 = gui.addFolder('camera');
	f3.add(gameparams, 'focusonball').onChange(function(value) {
		camfocusobject = value ? ball : null;	
	});
	

	gui.close();

	gui.remember(lightparams);
	
	
	//var fogcolor = 0x00ff00;

	scene = new THREE.Scene();
	//scene.background = new THREE.Texture( generateSkyTexture() );
	//scene.fog = new THREE.Fog(lightparams.amblightcolor, 400, 1020);

	// physics init
	initPhysics();

	// Grid
	//var gridHelper = new THREE.GridHelper( 100, 100 );
	//scene.add( gridHelper );	

	var balltexture = new THREE.TextureLoader().load( "http://i.imgur.com/ebeaxMb.png" );
	balltexture.wrapS = THREE.RepeatWrapping;
	balltexture.wrapT = THREE.RepeatWrapping;
	// balltexture.magFilter = THREE.NearestFilter;
	// balltexture.minFilter = THREE.NearestFilter;
	balltexture.repeat.set( 1, 1 );
	
    // gradient material	
	materials = {
		main: function( parameters, uniforms ) {	
			var m = new THREE.ShaderMaterial({
				vertexShader: shaderParse([
					"#include <shadowmap_pars_vertex>",
					"varying vec3 vNormal;",
					"varying vec4 vWorldPosition;",
					"varying vec2 vUv;",
					"void main() {",
						"#include <begin_vertex>",
						"#include <project_vertex>",
						"#include <worldpos_vertex>",
						"vWorldPosition = worldPosition;",
						"vNormal = normal;",
						"vUv = uv;",
						"#include <shadowmap_vertex>",
					"}"
				]),
				fragmentShader: shaderParse([
					"precision lowp float;",
					"uniform vec3 shadowcolor;",
					"uniform float opacity;",
					"uniform vec3 topcolor1;",
					"uniform vec3 topcolor2;",
					"uniform float topcolordiff;",
					"uniform vec3 color1;",
					"uniform float miny;",
					"uniform vec3 color2;",
					"uniform float maxy;",
					"uniform vec3 lightPosition;",
					"varying vec3 vNormal;",
					"varying vec4 vWorldPosition;",
					"#include <common>",
					"#include <packing>",
					"#include <bsdfs>",
					"#include <lights_pars>",
					"#include <shadowmap_pars_fragment>",
					"#include <shadowmask_pars_fragment>",

					"void main() {",
						"vec3 color;",
						"if (vNormal.y > 0.0) {",
							"color = mix(topcolor2, topcolor1, vNormal.y);",
							"color = color * (1.0 - (mod(floor(vWorldPosition.x*2.0) + mod(floor(vWorldPosition.z*2.0), 2.0), 2.0) * topcolordiff));",
						"} else {",
							"color = mix(color2, color1, max(min((vWorldPosition.y - miny) / (maxy - miny), 1.0), 0.0));",
						"}",
						"float shadowamount = (1.0 - min(getShadowMask()*10.0, 1.0)) * opacity;",
						"float backface = min(max(-dot(vNormal, normalize(lightPosition)), 0.0), 1.0) * 1.0;",

						"gl_FragColor = vec4( mix(shadowcolor, color, max(1.0 - backface - shadowamount, 0.0)), 1.0);",
						//"gl_FragColor = vec4( color, 1.0);",
					"}"
				]),
				uniforms: THREE.UniformsUtils.merge([THREE.ShaderLib['shadow'].uniforms, uniforms]),
			});
			m.setValues( parameters );
			return m;
		},
		ballshadow: function( parameters, uniforms ) {
			var m = new THREE.ShaderMaterial({
				vertexShader: shaderParse([
					"varying vec2 vUv;",
					"void main() {",
						"vUv = uv;",
						"gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);",
					"}"
				]),
				fragmentShader: shaderParse([
					"precision lowp float;",
					"uniform vec3 shadowcolor;",
					"uniform float opacity;",
					"varying vec2 vUv;",
					"void main() {",
						"if (0.5 < distance(vUv, vec2(0.5))) {",
							"gl_FragColor = vec4(0.0);",
						"} else {",
							"gl_FragColor = vec4(shadowcolor, opacity);",
						"}",
					"}"
				]),
				uniforms: uniforms,
			});
			m.setValues( parameters );
			return m;
		},
		ballmaterial: function( parameters, uniforms ) {
			var m = new THREE.ShaderMaterial({
				vertexShader: shaderParse([
					"varying vec2 vUv;",
					"void main() {",
						"vUv = uv;",
						"gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);",
					"}"
				]),
				fragmentShader: shaderParse([
					"precision lowp float;",
					"uniform sampler2D texture1;",
					"uniform vec3 color;",
					"uniform vec3 dotcolor;",
					"uniform float opacity;",
					"uniform float dotopacity;",
					"varying vec2 vUv;",
					"void main() {",
						"gl_FragColor = vec4(mix(color, dotcolor, dotopacity-(texture2D(texture1, vUv).x*dotopacity)), opacity);",
					"}"
				]),
				uniforms: uniforms,
			});
			m.setValues( parameters );
			return m;
		},
	};
	
	mainmaterial = new materials.main( {
			lights: true,
			fog: false,
			transparent: false
		},{
			opacity			: { type : "f", value : 0.15 },
			shadowcolor 	: { type : "c", value : new THREE.Color(lightparams.shadowcolor) },
			topcolor1 		: { type : "c", value : new THREE.Color(lightparams.topcolor1) },
			topcolor2 		: { type : "c", value : new THREE.Color(lightparams.topcolor2) },
			topcolordiff 	: { type : "f", value : 0.04 },
			color1			: { type : "c", value : new THREE.Color(lightparams.color1) },
			color2 			: { type : "c", value : new THREE.Color(lightparams.color2) },
			maxy 			: { type : "f", value : lightparams.colormaxy },
			miny 			: { type : "f", value : lightparams.colorminy },
			lightPosition	: {	type : "v3",value : new THREE.Vector3(1,1,1) }
		} 
	);

	ballshadowmaterial = new materials.ballshadow( {
			lights: false,
			fog: false,
			transparent: true,
			depthTest: true, 
			depthWrite: true, 
			polygonOffset: true,
			polygonOffsetFactor: -1,
		},{
			opacity		: { type : "f", value : 0.15 },
			shadowcolor : { type : "c", value : new THREE.Color(lightparams.shadowcolor) },
		} 
	);
	
	wireframematerial = new THREE.PointsMaterial({ 
		color: 0xffffff,
		depthTest: false, 
		depthWrite: false,
	});

	ballmaterial = new materials.ballmaterial( {
			lights: false,
			fog: false,
			transparent: true,
		},{
			color		: { type : "f", value : new THREE.Color("#ffffff") },
			dotcolor	: { type : "f", value : new THREE.Color(lightparams.shadowcolor) },
			texture1	: { type : "t", value : balltexture },
			opacity		: { type : "f", value : 1.0 },
			dotopacity	: { type : "f", value : 0.05 },
		} 
	);

	const lightdist = 128;
	const shadowresolution = 2048;

	dirLight = new THREE.DirectionalLight(0xffffff, 1);
	dirLight.color.setHSL( 0.1, 1, 0.95 );
	dirLight.position.set( -1, 1, 1 );
	mainmaterial.uniforms.lightPosition.value = dirLight.position;
	dirLight.castShadow = true;
	dirLight.shadow.mapSize.width = dirLight.shadow.mapSize.height = shadowresolution;
	dirLight.shadow.camera.far = lightdist;	
	dirLight.shadow.camera.near = -lightdist;

	scene.add( dirLight );
	
	//dirLight.shadow.bias = -0.00001;
	//dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 ) 
	//scene.add( dirLightHeper );

	//renderer = new THREE.WebGLRenderer({alpha: true});
	renderer = new THREE.WebGLRenderer({alpha: true});
	renderer.setSize( window.innerWidth, window.innerHeight );

	renderer.shadowMap.enabled = true;
	renderer.shadowMap.renderReverseSided = false;
	//renderer.shadowMap.renderSingleSided = true;
	//renderer.shadowMap.type = THREE.BasicShadowMap;
	renderer.shadowMap.type = THREE.PCFShadowMap;
	//renderer.shadowMap.type = THREE.PCFSoftShadowMap;
	
	document.getElementById("viewportwrapper").appendChild(renderer.domElement);

	
	// camera 
	createCameraRig();

	
	// stats
	var container = document.createElement('div');
	document.body.appendChild(container);
	// @ts-ignore
	stats = new Stats();
	container.appendChild(stats.dom);

	document.body.classList.add('maploaded');

	window.addEventListener( 'resize', onWindowResize, false );

	// camera controls
	window.addEventListener('wheel', onMouseWheel, false);
	window.addEventListener('mousemove', onMouseMove, false);
	window.addEventListener('mousedown', onMouseDown, false);
	window.addEventListener('mouseup', onMouseUp, false);
	window.addEventListener('contextmenu', onContextMenu, false);

	loadMap(mapdata);

	updateBgSky();	

}

function animate() {
	
	requestAnimationFrame( animate );

	updatePhysics();

	if (camfocusobject != null) {
		camfocuspoint.position.copy(camfocusobject.position);
	}

	
	//composer.render();
	renderer.render( scene, camera );

	stats.update();
}

function onWindowResize() {
	updateCameraAspect();
}

function updateBgSky(v) {
	if (skybgctx == null || lightparams == null) return;

	skybgctx.clearRect(0, 0, 100, 100);
	var gradient = skybgctx.createRadialGradient(50, lightparams.rady0+50, lightparams.radr0, 50, lightparams.rady0+50, lightparams.radr1);
	gradient.addColorStop(lightparams.skytopratio, lightparams.skytop);
	gradient.addColorStop(lightparams.skybottomratio, lightparams.skybottom);
	skybgctx.fillStyle = gradient;
	skybgctx.fillRect(0, 0, 100, 100);
}

function updateUniforms(v) {
	if (mainmaterial == null || mainmaterial.uniforms == null) return;

	mainmaterial.uniforms.topcolor1.value = new THREE.Color(lightparams.topcolor1);
	mainmaterial.uniforms.topcolor2.value = new THREE.Color(lightparams.topcolor2);
	mainmaterial.uniforms.color1.value = new THREE.Color(lightparams.color1);
	mainmaterial.uniforms.color2.value = new THREE.Color(lightparams.color2);
	mainmaterial.uniforms.shadowcolor.value = new THREE.Color(lightparams.shadowcolor);

	mainmaterial.uniforms.maxy.value = lightparams.colormaxy;
	mainmaterial.uniforms.miny.value = lightparams.colorminy;
}

////
// File loading

//function openFileDialog() {document.getElementById('fileinput').click();}
//document.getElementById('nothingopen').onclick = function() {openFileDialog();};

// Load selected json file data
function handleFileSelect(evt) {
	var files = evt.target.files; // FileList object
	// files is a FileList of File objects. List some properties.
	var output = [];
	for (var i = 0, f; f = files[i]; i++) {
		var reader = new FileReader();
		// Closure to capture the file information.
		reader.onload = (function (theFile) {
			return function (e) {
				console.log('e readAsText = ', e);
				console.log('e readAsText target = ', e.target);
				try {
					loadMap(JSON.parse(e.target.result));
				} catch (ex) {
					alert('ex when trying to load map = ' + ex);
				}
			}
		})(f);
		reader.readAsText(f);
	}
}
//document.getElementById('fileinput').addEventListener('change', handleFileSelect, false);

// loads map from json data
function loadMap(data) {

	var blocksx = data.blocks;
	var blockkeysx = Object.keys(blocksx);

	var blockgeometry = new THREE.Geometry();

	for (var xi = 0; xi < blockkeysx.length; xi++) {
		var xkey = blockkeysx[xi];
		var blocksz = blocksx[xkey];
		var blockkeysz = Object.keys(blocksz);

		for (var zi = 0; zi < blockkeysz.length; zi++) {
			var zkey = blockkeysz[zi];

			//blocks.push(createblock(xkey, zkey, blocksz[zkey], scene));
			blockgeometry.merge(getBlockGeom(xkey, zkey, data.blockdepth, blocksz[zkey], getBlockWalls(xkey,zkey,blocksx)));
		}
	}

	blockgeometry.mergeVertices();
	blockgeometry.computeFlatVertexNormals();
	blockgeometry.computeBoundingSphere();

	worldmesh = new THREE.Mesh( blockgeometry, mainmaterial );
	scene.add( worldmesh );
	worldmesh.castShadow = true;
	worldmesh.receiveShadow = true;
	worldmesh.matrixAutoUpdate = false;
	worldmesh.updateMatrix();

	// edges
	if (wireframe) {
		scene.add( 
			new THREE.LineSegments( 
				new THREE.WireframeGeometry(blockgeometry), 
				wireframematerial
			)
		);
	}

	var lbody = new CANNON.Body({
		mass: 0, // kg
		type: 1,
		position: worldmesh.position.toCannon(), // m
		shape: TrimeshfromGeo(blockgeometry),
		material: groundphymat
	});
	addBody(worldmesh, lbody, true);

	ball = new PlayerBall();
	ball.spawnpoint.set(0,1,0);
	ball.spawn(scene, worldmesh); 

	camfocusobject = ball;

	//console.log(ball);
}

// math
function clamp(val, min, max) {
	return Math.max(min, Math.min(max, val));
} 

function shaderParse(glsl) {
    return glsl.join("\n");
}



