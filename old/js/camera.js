////
// Camera

function createCameraRig() {
	var aspect = window.innerWidth / window.innerHeight;
	camera = new THREE.OrthographicCamera( zoomfustrum * aspect * -0.5, zoomfustrum * aspect * 0.5, zoomfustrum * 0.5, zoomfustrum * -0.5, -128, 128);
	//camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 5000 );

	camfocuspoint = new THREE.Group();
	camfocuspoint.position.set(0,0,0);
	camfocuspoint.rotation.set(0,Math.PI * 0.25,0);
	scene.add(camfocuspoint);

	camfocusrotation = new THREE.Group();
	camfocusrotation.position.set(0,0,0);
	//camfocusrotation.rotation.set(Math.PI * -0.5,0,0);

	var rotation = -30 * (Math.PI / 180);
	camfocusrotation.rotation.set(rotation,0,0);
	camfocusrotation.add(camera);
	camfocuspoint.add(camfocusrotation);
	
	camera.position.set(0,0,0); // how far away the camera is
}

// call this when the aspect, resolution, or zoom changes
function updateCameraAspect() {
	var aspect = window.innerWidth / window.innerHeight;
	camera.left = zoomfustrum * aspect * -0.5;
	camera.right = zoomfustrum * aspect * 0.5;
	camera.top = zoomfustrum * 0.5;
	camera.bottom = zoomfustrum * -0.5;

	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
}

const zoomspeed = 0.1;
const zoommax = 200;
const zoommin = 1;

function cameraZoom(amount) {
	zoomfustrum = clamp(zoomfustrum + ((amount * (zoomfustrum/zoommax)) * zoomspeed), zoommin, zoommax);
	updateCameraAspect();
}

// controls

const vecup = new THREE.Vector3(0,1,0);

const orbitspeed = 0.01;
var mousedownstart;
var aiming = false;
var aimdist = 0;

function onMouseWheel(e) {
	cameraZoom(e.deltaY);
}
function onMouseDown(e) {
    //console.log(e);
    mousedownstart = e.clientY;
}
function onMouseUp(e) {
    if (e.button == 0 && aiming) {
        aiming = false;

        var direction = new THREE.Vector3(0,0,-aimdist*0.001);

        direction.applyAxisAngle(vecup, camfocuspoint.rotation.y);

        camfocusobject.fire(direction.toCannon());
    }
}
function onMouseMove(e) {
	//console.log(e.buttons);
	
	if (e.buttons == 1) { // pan

        if (camfocusobject != null) {
            if (camfocusobject.type == 'PlayerBall' && camfocusobject.body.sleepState == 2) {


                aiming = true;
                aimdist = Math.max(-(mousedownstart - e.clientY), 0);

                camfocuspoint.rotation.y -= e.movementX * orbitspeed;                
                
            } else {
                return;                
            }
        }
		var movement = screentoworldscale(-e.movementX, -e.movementY);

		camfocuspoint.position.add(movement);

	} else if (e.buttons == 2) { // orbit
		camfocuspoint.rotation.y -= e.movementX * orbitspeed;

	} else if (e.buttons == 3) { // zoom
		cameraZoom(e.movementY*5);
	}
}
function onContextMenu(e) {
	e.preventDefault();
	return false;
}


function screentoworldscale(x, y) {
	
	//x /= window.innerWidth;
	//y /= window.innerHeight;

	//x *= camera.right*2;
	//y *= camera.top*2;

	//y /= -Math.sin(camfocusrotation.rotation.x);
	
	var vec = new THREE.Vector3(
		(x / window.innerWidth) * camera.right*2,
		0,
		((y / window.innerHeight) * camera.top*2) / -Math.sin(camfocusrotation.rotation.x)
	);

	// apply rotation
	vec.applyAxisAngle(vecup, camfocuspoint.rotation.y);
	
	return vec;
}