

function PlayerBall() {

    // inherit Object3D
    THREE.Object3D.call(this);

    this.type = 'PlayerBall';

    this.size = 0.05;
    this.spawnpoint = new THREE.Vector3(0,0,0);
    
    this.mesh;
    this.body;
    this.shadowdecal;
    this.worldmesh;
    this.shadowpos;
}

const shadowdir = new THREE.Vector3(Math.PI*0.5,0,0);
const shadowdist = 6;

PlayerBall.prototype = Object.assign(Object.create(THREE.Object3D.prototype), {
    constructor: PlayerBall,

    spawn: function(scene, wmesh) {
        this.position.copy(this.spawnpoint);      
        this.worldmesh = wmesh;  

        var geo = new THREE.SphereBufferGeometry(this.size, 16, 8);

        this.mesh = new THREE.Mesh(geo, ballmaterial);
        this.mesh.castShadow = false;
        this.mesh.receiveShadow = true;
        this.add(this.mesh);
        
        this.body = new CANNON.Body({
            mass: 0.05, // kg
            position: this.spawnpoint.toCannon(),
            shape: new CANNON.Sphere(this.size),
            allowSleep: true,
            sleepSpeedLimit: 0.14,
            linearDamping: 0.3,
            angularDamping: 0.5,
            material: ballphymat
        });
        this.body.addEventListener("sleep", this.sleep);

        this.shadowdecal = new THREE.Mesh( 
            new THREE.Geometry(), ballshadowmaterial 
        );
        scene.add(this.shadowdecal);
        this.shadowpos = new THREE.Vector3(0,0,0);
        this.shadowscale = new THREE.Vector3(this.size*2,this.size*2,shadowdist);
        
        scene.add(this);
        addBody(this, this.body, false, false);        
    },

    sleep: function(v) {
        console.log(v);
    },

    respawn: function(scene) {
        this.body.position = this.spawnpoint.toCannon();
        this.body.sleep();
        this.body.wakeUp();
    },

    fire: function(force) {
        this.body.wakeUp();
        this.body.applyImpulse(force, this.body.position);
    },

    update: function() {
        this.updateShadowDecal();
        //console.log(this.shadowdecal.position);
    },

    updateShadowDecal: function() {
        this.shadowdecal.geometry = new THREE.DecalGeometry(
            this.worldmesh, // it has to be a THREE.Mesh
            new THREE.Vector3(this.position.x, this.position.y - shadowdist*0.5, this.position.z), // world position 
            shadowdir, // direction 
            this.shadowscale, // scale
            //check // THREE.Vector3 specifying what sides to clip (1-clip, 0-noclip)  
        );
        this.shadowdecal.geometry.verticesNeedUpdate = true;
    }
});