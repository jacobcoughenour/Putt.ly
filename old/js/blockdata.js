const meshdata = {

    // base x and z values of verts
    // 0 - 3
    // | 4 |
    // 1 - 2
    verts: [-0.5,0.5, 0.5,0.5, 0.5,-0.5, -0.5,-0.5, 0,0],

    // y added to verts of each surface corner
    // 0 - 3
    // |(4)|
    // 1 - 2
    sy: [
        [false,false,false,false],
        [false,false,false,false,true],
        [false,true ,false,false],
        [false,true ,false,false],
        [true ,true ,false,false],
        [true ,false,true ,false],
        [true ,false,true ,false],
        [true ,true ,true ,false],
        [true ,true ,true ,false]
    ],

    // surface triangles
    // 0 - 3
    // | 4 |
    // 1 - 2
    stri: [
        [[0,1,3], [1,2,3]],
        [[0,1,4], [1,2,4], [4,3,0], [2,3,4]],
        [[0,1,3], [1,2,3]],
        [[0,2,3], [2,0,1]],
        [[0,2,3], [2,0,1]],
        [[0,2,3], [2,0,1]],
        [[0,1,3], [1,2,3]],
        [[0,2,3], [2,0,1]],
        [[0,1,3], [1,2,3]]
    ],
};